require 'spec_helper'

describe Bio::Picard do
	it 'has a version number' do
		expect(Bio::Picard::VERSION).not_to be nil
	end

	it 'does something useful' do
		expect(false).to eq(true)
	end
	
	describe "#run" do
		before {@picard = Bio::Picard.new()}
		context 'run picard help' do
			it {expect(@picard.run('-help')).not_to be_empty}
		end
	end
end
