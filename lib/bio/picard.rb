require "bio/picard/version"

module Bio

	class Picard
		
		# instantiate Picard command tools
		# @param [String] path path to picard.jar
		def initialize(*java_options)
			@java_options = java_options.join(' ')
			@picard = File.join(File.expand_path(File.dirname(__FILE__)), 'picard', 'external', 'picard.jar')
		end
		
		# run picard tools for command and all options please go to 
		# http://broadinstitute.github.io/picard/command-line-overview.html
		#
		# @param [String] command Picard command
		# @param [Array] options of Picard command
		def run(command, *options)
			`java #{@java_options == '' ? '' : "#{@java_options.chomp} "}-jar #{@picard} #{command} #{options.flatten.join(' ')}`
		end
	end
end
