# coding: utf-8

path = File.expand_path(File.dirname(__FILE__))

require '../lib/bio/picard/version'
version = Bio::Picard::VERSION.split(/\./)[0..1].join('.')

File.open(File.join(path,"Rakefile"),"w") do |rakefile|
rakefile.write <<-RAKE
require 'rbconfig'
require 'open-uri'
require 'fileutils'
include FileUtils::Verbose
require 'rake/clean'


task :picard do
	
	open("https://github.com/broadinstitute/picard/archive/#{version}.zip") do |uri|
		File.open("picard.zip",'wb') do |fout|
			fout.write(uri.read)
		end #fout 
	end #uri
	
	sh "unzip picard.zip"
	rm "picard.zip"
	mv "picard-#{version}", "picard"
	
	open("https://github.com/samtools/htsjdk/archive/#{version}.zip") do |uri|
		File.open("htsjdk.zip",'wb') do |fout|
			fout.write(uri.read)
		end #fout 
	end #uri
	
	sh "unzip htsjdk.zip"
	rm "htsjdk.zip"
	mv "htsjdk-#{version}", "picard/htsjdk"
	
	cd "picard" do
		sh "ant -lib lib/ant package-commands"
	end
	mkdir "../lib/bio/picard/external"
	mv "picard/dist/picard.jar", "../lib/bio/picard/external"
	rm_rf "picard"
end

task :default => [:picard]
RAKE

end